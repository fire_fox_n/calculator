<?php
namespace App\library\calculator;

interface EvaluableInterface
{
    public function evaluate();
}
