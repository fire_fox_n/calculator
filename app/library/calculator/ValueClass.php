<?php
namespace App\library\calculator;

class ValueClass implements EvaluableInterface {
    private $value;
    
    public function __construct($value)
    {
        $this->value = $value;
    }
    
    public function evaluate()
    {
        return $this->value;
    }
}
