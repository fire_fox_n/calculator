<?php
namespace App\library\calculator\operations;

use App\library\calculator\EvaluableInterface;

abstract class DetailAbstractClass {
    /* @var $left EvaluableInterface */
    protected $left;
    /* @var $right EvaluableInterface */
    protected $right;
    
    public function __construct(EvaluableInterface $left, EvaluableInterface $right)
    {
        $this->left = $left;
        $this->right = $right;
    }
}
