<?php
namespace App\library\calculator\operations;

use App\library\calculator\EvaluableInterface;

class MultiplyClass extends DetailAbstractClass implements EvaluableInterface {
    public function evaluate()
    {   
        return $this->left->evaluate() * $this->right->evaluate();
    }
}
