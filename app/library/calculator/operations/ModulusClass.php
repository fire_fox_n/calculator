<?php

namespace App\library\calculator\operations;

use App\library\calculator\EvaluableInterface;

class ModulusClass extends DetailAbstractClass implements EvaluableInterface {
    public function evaluate() 
    {
        if ($this->right->evaluate() === 0) {
            return $this->left->evaluate();
        }

        return $this->left->evaluate() % $this->right->evaluate();
    }
}
