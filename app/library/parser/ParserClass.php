<?php

namespace App\library\parser;

class ParserClass {
    
    private $file;
    private $arrRows;
    private $arrRes;


    /**
     * @param string $filename
     */
    public function setFile($filename)
    {
        $this->file = $filename;
    }

    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function parse()
    {
        if ($this->isEmptyString($this->file)) {
            throw new EmptyPathFileException('empty argument');
        }

        if (file_exists($this->file)) {
            $f = fopen($this->file, 'r');
        } else {
            throw new FileNotFoundException('file is not exist');
        }
        
        $this->arrRows = [];
        while ($str = trim(fgets($f))) {
            if (!$this->isEmptyString($str)) {
                $this->arrRows[] = $str;
            }
        }
        
        fclose($f);
        
        $this->makeArrayWithOperations();
        
        return $this->arrRes;
    }
    
    private function makeArrayWithOperations()
    {
        $str = array_pop($this->arrRows);
        $arrFirstValue = $this->getArrayAction($str);
        
        if ($arrFirstValue['action'] == 'apply') {
            $this->arrRes[$arrFirstValue['action']] = $arrFirstValue['value'];
        }
        
        if (!empty($this->arrRows)) {
            foreach ($this->arrRows as $row) {
                $arrOperator = $this->getArrayAction($row);
                $this->arrRes[$arrOperator['action']] = $arrOperator['value'];
            }
        }
    }
    
    private function getArrayAction($str)
    {
        $arrStr = explode(' ', $str);
        $res = [
            'action' => $arrStr[0],
            'value' => $arrStr[1],
        ];
        
        return $res;
    }

    private function isEmptyString($str)
    {
        return empty($str);
    }
}
