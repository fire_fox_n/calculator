<?php
namespace App\library;

use App\library\calculator\operations\AdditionClass;
use App\library\calculator\ValueClass;
use App\library\calculator\operations\MultiplyClass;
use App\library\calculator\operations\SubstractionClass;

class AdapterClass {
    private $arrNodes;

    public function __construct()
    {
        $this->arrNodes = [];
    }
    
    public function setNodes(array $arr)
    {
        $this->arrNodes = $arr;
    }

    public function calc()
    {
        if (empty($this->arrNodes)) {
            throw new WrongDataException();
        }
        $left = new ValueClass(0);
        
        if (array_key_exists('apply', $this->arrNodes)) {
            foreach ($this->arrNodes as $key => $value) {
                
                $valObj = new ValueClass($value);
                
                switch ($key) {
                    case 'apply':
                        $left = $valObj;
                        break;
                    case 'add':
                        $result = new AdditionClass($left, $valObj);
                        $left = $result;
                        break;
                    case 'multiply';
                        $result = new MultiplyClass($left, $valObj);
                        $left = $result;
                        break;
                    case 'substract':
                        $result = new SubstractionClass($left, $valObj);
                        break;
                    default:
                        break;
                }
                
            }
            
            return $left->evaluate();
        } else {
            throw new WrongDataException();
        }
        
    }
}
