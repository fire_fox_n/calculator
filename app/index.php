<?php
use App\library\calculator\operations\AdditionClass;
use App\library\calculator\operations\SubstractionClass;
use App\library\calculator\ValueClass;
use App\library\parser\ParserClass;
use App\library\AdapterClass;

require_once __DIR__.'/../vendor/autoload.php';

$files = glob('files/*.txt');
$files or exit('No files to check.'.PHP_EOL);

$parser = new ParserClass();
$adapter = new AdapterClass();
foreach ($files as $file) {
    $parser->setFile(__DIR__.'/../'.$files[0]);
    echo 'Parse file: '.$file.PHP_EOL;
    $arrRes = $parser->parse();
    print_r($arrRes);
    echo PHP_EOL;
    echo 'Result is: ';


    $adapter->setNodes($arrRes);
    $calculatedRes = $adapter->calc();
    echo $calculatedRes.PHP_EOL;
}
