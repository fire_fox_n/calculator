<?php
use App\library\calculator\operations\AdditionClass;
use App\library\calculator\ValueClass;
use App\library\calculator\EvaluableInterface;

class AdditionClassTest extends \PHPUnit\Framework\TestCase
{
    public function testCheckEvaluableInterface()
    {
        $summ = new AdditionClass(new ValueClass(3), new ValueClass(4));

        $this->assertInstanceOf(EvaluableInterface::class, $summ);
    }

    public function testCheckAddition()
    {
        $summ = new AdditionClass(new ValueClass(3), new ValueClass(4));

        $this->assertEquals(7, $summ->evaluate());
    }
}