<?php
use PHPUnit\Framework\TestCase;
use App\library\calculator\EvaluableInterface;
use App\library\calculator\ValueClass;
use App\library\calculator\operations\MultiplyClass;

class MultiplyClassTest extends TestCase
{
    public function testCheckEvaluableInterface()
    {
        $mult = new MultiplyClass(new ValueClass(3), new ValueClass(4));

        $this->assertInstanceOf(EvaluableInterface::class, $mult);
    }

    public function testCheckMultiple()
    {
        $mult = new MultiplyClass(new ValueClass(3), new ValueClass(4));

        $this->assertEquals(12, $mult->evaluate());
    }
}