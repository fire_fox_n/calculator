<?php
use PHPUnit\Framework\TestCase;
use App\library\calculator\operations\SubstractionClass;
use App\library\calculator\ValueClass;
use App\library\calculator\EvaluableInterface;

class SubstractionClassTest extends TestCase
{
    public function testCheckEvaluableInterface()
    {
        $sub = new SubstractionClass(new ValueClass(3), new ValueClass(4));

        $this->assertInstanceOf(EvaluableInterface::class, $sub);
    }

    public function testCheckSubstraction()
    {
        $sub = new SubstractionClass(new ValueClass(3), new ValueClass(4));

        $this->assertEquals(-1, $sub->evaluate());
    }
}