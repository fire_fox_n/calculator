<?php

use PHPUnit\Framework\TestCase;
use App\library\calculator\ValueClass;

class ValueClassTest extends TestCase
{
    public function testCreateValue()
    {
        $objValue = new ValueClass(6);

        $this->assertEquals(6, $objValue->evaluate());
    }
}