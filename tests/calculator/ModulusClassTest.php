<?php
use PHPUnit\Framework\TestCase;
use App\library\calculator\EvaluableInterface;
use App\library\calculator\operations\ModulusClass;
use App\library\calculator\ValueClass;

class ModulusClassTest extends TestCase
{
    public function testCheckEvaluableInterface()
    {
        $mod = new ModulusClass(new ValueClass(10), new ValueClass(2));

        $this->assertInstanceOf(EvaluableInterface::class, $mod);
    }

    public function testCheckModulus()
    {
        $mod = new ModulusClass(new ValueClass(10), new ValueClass(2));

        $this->assertEquals(0, $mod->evaluate());
    }

    public function testSkipZero()
    {
        $mod = new ModulusClass(new ValueClass(10), new ValueClass(0));

        $this->assertEquals(10, $mod->evaluate());
    }
}