<?php
use PHPUnit\Framework\TestCase;
use App\library\AdapterClass;
use App\library\WrongDataException;

class AdapterClassTest extends TestCase
{
    /** @var  $adapter AdapterClass */
    private $adapter;

    public function setUp()
    {
        $this->adapter = new AdapterClass();
    }

    public function testCheckEmptyArray()
    {
        $this->expectException(WrongDataException::class);
        $this->adapter->calc();
    }

    public function testArrayWithoutApply()
    {
        $this->expectException(WrongDataException::class);

        $arr = [
            'add' => 2,
            'multiple' => 3
        ];

        $this->adapter->setNodes($arr);
        $this->adapter->calc();
    }

    public function testCalc()
    {
        $arr = [
            'apply' => 4,
            'add' => 2,
            'multiply' => 3
        ];

        $this->adapter->setNodes($arr);
        $res = $this->adapter->calc();

        $this->assertEquals(18, $res);
    }
}