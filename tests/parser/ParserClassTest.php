<?php
use PHPUnit\Framework\TestCase;
use App\library\parser\ParserClass;
use App\library\parser\EmptyPathFileException;
use App\library\parser\FileNotFoundException;

class ParserClassTest extends TestCase
{
    /** @var  $parser ParserClass */
    private $parser;

    public function setUp()
    {
        $this->parser = new ParserClass();
    }

    public function testGetFile()
    {
        $this->parser->setFile('file.txt');

        $this->assertEquals('file.txt', $this->parser->getFile());
    }

    public function testCheckEmptyFilePath()
    {
        $this->expectException(EmptyPathFileException::class);
        $this->parser->parse();
    }

    public function testCheckNotFoundFile()
    {
        $this->expectException(FileNotFoundException::class);
        $this->parser->setFile('file.txt');
        $this->parser->parse();
    }

    public function testChectResult()
    {
        $this->parser->setFile(__DIR__.'/../../files/file_01.txt');
        //echo __DIR__.'/../files/file_01.txt'.PHP_EOL;
        $arrRes = $this->parser->parse();
        $this->assertArrayHasKey('apply', $arrRes);
    }
}